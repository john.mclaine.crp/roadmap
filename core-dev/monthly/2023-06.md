# June 2023

## Developers

* [Ethan Reesor](https://gitlab.com/firelizzard)
* [Dennis Bunfield](https://gitlab.com/bunfield)
* [Paul Snow](https://gitlab.com/PaulSnow)

## Work planned

- **Core 1.2**
  - Review and test Executor v2.
  - Minor improvements to API v3 to create a better UX for explorer users.
  - Implement rejections and abstensions.
  - Complete snapshot improvements.
  - Implement block thresholds (nice to have).
  - Implement rejection of pending transactions older than 2 weeks (nice to
    have).
- **Staking**
  - Add support for more types of actions.
  - Develop an improved consensus and validator design.
  - Explore validator development kit concepts.
  - Continue work on mining.
- **Wallets**
  - Add dedicated commands for staking actions.
  - Polish support for using multiple vaults.
  - Support running the daemon as a separate, long-running process.
